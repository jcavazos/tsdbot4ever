package org.tsd.tsdbot4ever;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;

import java.net.URI;

@Slf4j
@Configuration
public class DatabaseConfig {
    @Bean
    public DynamoDbClient dynamoDbClient(@Value("${aws.dynamodb.endpoint}") String dynamoDbEndpoint,
                                         @Value("${aws.dynamodb.region}") Region region) {
        log.info("Dynamo DB URL: {}", dynamoDbEndpoint);
        return DynamoDbClient
                .builder()
                .region(region)
                .endpointOverride(URI.create(dynamoDbEndpoint))
                .build();
    }
}
