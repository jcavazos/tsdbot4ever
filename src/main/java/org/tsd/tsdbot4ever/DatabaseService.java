package org.tsd.tsdbot4ever;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;

@Service
public class DatabaseService {

    private final DynamoDbClient dynamoDbClient;

    @Autowired
    public DatabaseService(DynamoDbClient dynamoDbClient) {
        this.dynamoDbClient = dynamoDbClient;
    }
}
