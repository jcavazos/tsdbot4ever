FROM maven:3.8.7-openjdk-18 AS build
COPY . /app
WORKDIR /app
RUN mvn clean package -DskipTests

FROM openjdk:22-jdk
COPY --from=build /app/target/tsdbot4ever-0.0.1-SNAPSHOT.jar /app.jar

ARG ARG_DISCORD_BOT_TOKEN
ARG ARG_DYNAMODB_ENDPOINT

ENV DISCORD_BOT_TOKEN=${ARG_DISCORD_BOT_TOKEN}
ENV DYNAMODB_ENDPOINT=${ARG_DYNAMODB_ENDPOINT}

ENTRYPOINT ["java","-jar","/app.jar"]