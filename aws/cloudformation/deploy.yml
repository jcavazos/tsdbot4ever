AWSTemplateFormatVersion: "2010-09-09"
Description: CloudFormation template for the clustered sample app

Parameters:
  Image:
    Type: String
  Environment:
    Type: String
    AllowedValues:
      - dev
      - prod
  ContainerPort:
    Type: Number
    Default: 8080
  LoadBalancerPort:
    Type: Number
    Default: 80
  HealthCheckPath:
    Type: String
    Default: /healthcheck
  MinContainers:
    Type: Number
    Default: 1
  MaxContainers:
    Type: Number
    Default: 1

Resources:
  # A VPC for this application
  VPC:
    Type: "AWS::EC2::VPC"
    Properties:
      EnableDnsSupport: "true"
      EnableDnsHostnames: "true"
      CidrBlock: "10.0.0.0/16"

  PrivateSubnet0:
    Type: "AWS::EC2::Subnet"
    Properties:
      VpcId:
        Ref: "VPC"
      AvailabilityZone: !Sub "${AWS::Region}a"
      CidrBlock: "10.0.2.0/24"

  PrivateSubnet1:
    Type: "AWS::EC2::Subnet"
    Properties:
      VpcId:
        Ref: "VPC"
      AvailabilityZone: !Sub "${AWS::Region}b"
      CidrBlock: "10.0.3.0/24"

  InternetGateway:
    Type: "AWS::EC2::InternetGateway"

  GatewayToInternet:
    Type: "AWS::EC2::VPCGatewayAttachment"
    Properties:
      VpcId:
        Ref: "VPC"
      InternetGatewayId:
        Ref: "InternetGateway"

  PublicRouteTable:
    Type: "AWS::EC2::RouteTable"
    Properties:
      VpcId:
        Ref: "VPC"

  PublicRoute:
    Type: "AWS::EC2::Route"
    DependsOn: "GatewayToInternet"
    Properties:
      RouteTableId:
        Ref: "PublicRouteTable"
      DestinationCidrBlock: "0.0.0.0/0"
      GatewayId:
        Ref: "InternetGateway"

  PublicNetworkAcl:
    Type: "AWS::EC2::NetworkAcl"
    Properties:
      VpcId:
        Ref: "VPC"

  OutboundPublicNetworkAclEntry:
    Type: "AWS::EC2::NetworkAclEntry"
    Properties:
      NetworkAclId:
        Ref: "PublicNetworkAcl"
      RuleNumber: "100"
      Protocol: "-1"
      RuleAction: "allow"
      Egress: "true"
      CidrBlock: "0.0.0.0/0"
      PortRange:
        From: "0"
        To: "65535"

  PrivateRouteTable0:
    Type: "AWS::EC2::RouteTable"
    Properties:
      VpcId:
        Ref: "VPC"

  PrivateRouteTable1:
    Type: "AWS::EC2::RouteTable"
    Properties:
      VpcId:
        Ref: "VPC"

  PrivateRouteToInternet0:
    Type: "AWS::EC2::Route"
    Properties:
      RouteTableId:
        Ref: "PrivateRouteTable0"
      DestinationCidrBlock: "0.0.0.0/0"
      NatGatewayId:
        Ref: "NATGateway0"

  PrivateRouteToInternet1:
    Type: "AWS::EC2::Route"
    Properties:
      RouteTableId:
        Ref: "PrivateRouteTable1"
      DestinationCidrBlock: "0.0.0.0/0"
      NatGatewayId:
        Ref: "NATGateway1"

  PrivateSubnetRouteTableAssociation0:
    Type: "AWS::EC2::SubnetRouteTableAssociation"
    Properties:
      SubnetId:
        Ref: "PrivateSubnet0"
      RouteTableId:
        Ref: "PrivateRouteTable0"

  PrivateSubnetRouteTableAssociation1:
    Type: "AWS::EC2::SubnetRouteTableAssociation"
    Properties:
      SubnetId:
        Ref: "PrivateSubnet1"
      RouteTableId:
        Ref: "PrivateRouteTable1"

  TsdbotTable:
    Type: AWS::DynamoDB::Table
    Properties:
      KeySchema:
        - AttributeName: "PK"
          KeyType: "S"
        - AttributeName: "SK"
          KeyType: "S"
      ProvisionedThroughput:
        ReadCapacityUnits: 5
        WriteCapacityUnits: 5
      TableName: !Join ['', [!Ref "TSDBOT4_", !Ref Environment, "_TABLE"]]

  # The ECS Cluster
  Cluster:
    Type: AWS::ECS::Cluster
    Properties:
      ClusterName: !Join ['', ["tsdbot4-", !Ref Environment, Cluster]]
      ClusterSettings:
        - Name: containerInsights
          Value: enabled

  # A role needed by ECS
  ExecutionRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Join ['', ["tsdbot4-", !Ref Environment, ExecutionRole]]
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: ecs-tasks.amazonaws.com
            Action: 'sts:AssumeRole'
      ManagedPolicyArns:
        - 'arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy'

  # A role for the containers (with inline policy for accessing the DDB table)
  TaskRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Join ['', ["tsdbot4-", !Ref Environment, TaskRole]]
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: ecs-tasks.amazonaws.com
            Action: 'sts:AssumeRole'
      Policies:
        - PolicyName: ddbClusterNodeTablePolicy
          PolicyDocument:
            Version: '2012-10-17'
            Statement:
              - Effect: Allow
                Action:
                  - 'dynamodb:Scan'
                  - 'dynamodb:UpdateItem'
                  - 'dynamodb:DeleteItem'
                  - 'dynamodb:PutItem'
                Resource: !Join ['', ['arn:aws:dynamodb:*:*:table/', !Ref TsdbotTable]]

  TaskDefinition:
    Type: AWS::ECS::TaskDefinition
    Properties:
      Family: !Join ['', ["tsdbot4-", !Ref Environment, "-", TaskDefinition]]
      # awsvpc is required for Fargate
      NetworkMode: awsvpc
      RequiresCompatibilities:
        - FARGATE
      Cpu: 256
      Memory: 0.5GB
      ExecutionRoleArn: !GetAtt ExecutionRole.Arn
      TaskRoleArn: !GetAtt  TaskRole.Arn
      ContainerDefinitions:
        - Name: !Join ['', ["tsdbot4-", container]]
          Image: !Ref Image
          PortMappings:
            - ContainerPort: !Ref ContainerPort
          # Send logs to CloudWatch Logs
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-region: !Ref AWS::Region
              awslogs-group: !Ref LogGroup
              awslogs-stream-prefix: ecs

  # A role needed for auto scaling
  AutoScalingRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Join ['', ["tsdbot4-", !Ref Environment, "-", AutoScalingRole]]
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: ecs-tasks.amazonaws.com
            Action: 'sts:AssumeRole'
      ManagedPolicyArns:
        - 'arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceAutoscaleRole'

  ContainerSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: !Join ['', ["tsdbot4-", !Ref Environment, "-", ContainerSecurityGroup]]
      VpcId: !Ref VPC
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: !Ref ContainerPort
          ToPort: !Ref ContainerPort
          SourceSecurityGroupId: !Ref LoadBalancerSecurityGroup

  # This loadbalancer will be exposed publicly without any restriction
  LoadBalancerSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: !Join ['', ["tsdbot4-", !Ref Environment, "-", LoadBalancerSecurityGroup]]
      VpcId: !Ref VPC
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: !Ref LoadBalancerPort
          ToPort: !Ref LoadBalancerPort
          CidrIp: 0.0.0.0/0

  Service:
    Type: AWS::ECS::Service
    # This dependency is needed so that the load balancer is setup correctly in time
    DependsOn:
      - ListenerHTTP
    Properties:
      ServiceName: !Join ['', ["tsdbot4-", !Ref Environment]]
      Cluster: !Ref Cluster
      TaskDefinition: !Ref TaskDefinition
      DeploymentConfiguration:
        MinimumHealthyPercent: 100
        MaximumPercent: 200
      DesiredCount: 2
      # This may need to be adjusted if the container takes a while to start up
      HealthCheckGracePeriodSeconds: 60
      LaunchType: FARGATE
      NetworkConfiguration:
        AwsvpcConfiguration:
          # change to DISABLED if you're using private subnets that have access to a NAT gateway
          AssignPublicIp: DISABLED
          Subnets:
            - !Ref PrivateSubnet0
            - !Ref PrivateSubnet1
          SecurityGroups:
            - !Ref ContainerSecurityGroup
      LoadBalancers:
        - ContainerName: !Join ['', ["tsdbot4-", !Ref Environment]]
          ContainerPort: !Ref ContainerPort
          TargetGroupArn: !Ref TargetGroup

  TargetGroup:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    Properties:
      HealthCheckIntervalSeconds: 30
      # will look for a 200 status code by default unless specified otherwise
      HealthCheckPath: !Ref HealthCheckPath
      HealthCheckTimeoutSeconds: 5
      UnhealthyThresholdCount: 2
      HealthyThresholdCount: 2
      Name: !Join ['', ["tsdbot4-", !Ref Environment, "-", TargetGroup]]
      Port: !Ref ContainerPort
      Protocol: HTTP
      TargetGroupAttributes:
        - Key: deregistration_delay.timeout_seconds
          Value: 60 # default is 300
      TargetType: ip
      VpcId: !Ref VPC

  ListenerHTTP:
    Type: AWS::ElasticLoadBalancingV2::Listener
    Properties:
      DefaultActions:
        - TargetGroupArn: !Ref TargetGroup
          Type: forward
      LoadBalancerArn: !Ref LoadBalancer
      Port: !Ref LoadBalancerPort
      Protocol: HTTP

  LoadBalancer:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Properties:
      LoadBalancerAttributes:
        # this is the default, but is specified here in case it needs to be changed
        - Key: idle_timeout.timeout_seconds
          Value: 60
      Name: !Join ['', ["tsdbot4-", !Ref Environment, "-", LoadBalancer]]
      # "internet-facing" is also an option
      Scheme: internal
      SecurityGroups:
        - !Ref LoadBalancerSecurityGroup
      Subnets:
        - !Ref PrivateSubnet0
        - !Ref PrivateSubnet1

  LogGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Join ['', [/ecs/, "tsdbot4-", !Ref Environment, "-", TaskDefinition]]

  AutoScalingTarget:
    Type: AWS::ApplicationAutoScaling::ScalableTarget
    Properties:
      MinCapacity: !Ref MinContainers
      MaxCapacity: !Ref MaxContainers
      ResourceId: !Join ['/', [service, !Ref Cluster, !GetAtt Service.Name]]
      ScalableDimension: ecs:service:DesiredCount
      ServiceNamespace: ecs
      RoleARN: !GetAtt AutoScalingRole.Arn

  AutoScalingPolicy:
    Type: AWS::ApplicationAutoScaling::ScalingPolicy
    Properties:
      PolicyName: !Join ['', ["tsdbot4-", !Ref Environment, "-", AutoScalingPolicy]]
      PolicyType: TargetTrackingScaling
      ScalingTargetId: !Ref AutoScalingTarget
      TargetTrackingScalingPolicyConfiguration:
        PredefinedMetricSpecification:
          PredefinedMetricType: ECSServiceAverageCPUUtilization
        ScaleInCooldown: 10
        ScaleOutCooldown: 10
        # Keep things at or lower than 50% CPU utilization, for example
        TargetValue: !Ref AutoScalingTargetValue

Outputs:
  ClusteredApplicationURL:
    Description: The URL to access the application
    Value: !GetAtt LoadBalancer.DNSName